filetype plugin indent on
filetype indent on
set nocompatible              " be iMproved, required
filetype off                  " required


syntax on
colorscheme srcery
hi Normal ctermbg=none


set relativenumber
set expandtab
set autoindent
set showmatch
set cursorline
set ruler
set showcmd
set showmode

set laststatus=2
set tabstop=4
set shiftwidth=4
set softtabstop=4
set textwidth=79
set fileformat=unix
set encoding=utf-8
set shortmess+=I

let python_highlight_all=1

set printfont=:h9
set printoptions=paper:A4,number:y,left:5pc

set tags=~/mytags
